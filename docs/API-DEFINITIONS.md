API Definitions
===

# Why?

Because it's good to gather all this info that we find in one place.

# Who?

For anyone that is interested or wants to help with

# Sources

- https://github.com/endor-force/home-assistant-garo-chargebox/blob/main/garo_chargebox/pkg_garo_chargebox.yaml

# Definitions

- /servlet/rest/chargebox/status
- /servlet/rest/chargebox/mode/[ALWAYS_ON,ALWAYS_OFF,SCHEMA]

## Example from /servlet/rest/chargebox/status

```json
{
  "serialNumber": SERIALNUMBER,
  "ocppState": null,
  "connectedToInternet": false,
  "freeCharging": false,
  "ocppConnectionState": null,
  "connector": "CHARGING",
  "mode": "SCHEMA",
  "currentLimit": 40,
  "factoryCurrentLimit": 32,
  "switchCurrentLimit": 20,
  "powerMode": "ON",
  "currentChargingCurrent": 9000,
  "currentChargingPower": 6100,
  "accSessionEnergy": 349,
  "sessionStartTime": SESSOIN_START_TIME_EPOCH,
  "chargeboxTime": "DATETIME",
  "accSessionMillis": 194665,
  "latestReading": 2592515,
  "chargeStatus": 64,
  "updateStatus": {
    "serialsToUpdate": [],
    "serialsUpdated": [],
    "currentlyUpdating": -1,
    "currentProgress": -1,
    "failedUpdate": "NO_ERROR"
  },
  "currentTemperature": 21,
  "sessionStartValue": 2592166,
  "nrOfPhases": 3,
  "slaveControlWarning": false,
  "supportConnectionEnabled": false,
  "datetimeConfigured": true,
  "pilotLevel": 10,
  "mainCharger": {
    "reference": null,
    "serialNumber": SERIALNUMBER,
    "lastContact": 1671706638051,
    "online": false,
    "loadBalanced": false,
    "phase": 0,
    "productId": 132,
    "meterStatus": 0,
    "meterSerial": "4136453",
    "chargeStatus": 64,
    "pilotLevel": 10,
    "accEnergy": 2592515,
    "firmwareVersion": 8,
    "firmwareRevision": 4,
    "wifiCardStatus": 2,
    "connector": "CHARGING",
    "accSessionEnergy": 349,
    "sessionStartValue": 2592166,
    "accSessionMillis": 194665,
    "sessionStartTime": 1671706443385,
    "currentChargingCurrent": 9000,
    "currentChargingPower": 6100,
    "nrOfPhases": 3,
    "twinSerial": 2309714,
    "cableLockMode": 0,
    "minCurrentLimit": 6,
    "dipSwitchSettings": 2746,
    "cpuType": 2004993,
    "updateable": false
  },
  "twinCharger": {
    "reference": null,
    "serialNumber": SERIALNUMBER,
    "lastContact": 1671706636617,
    "online": false,
    "loadBalanced": false,
    "phase": 0,
    "productId": 104,
    "meterStatus": 0,
    "meterSerial": "4137816",
    "chargeStatus": 64,
    "pilotLevel": 10,
    "accEnergy": 1526243,
    "firmwareVersion": 8,
    "firmwareRevision": 4,
    "wifiCardStatus": 1,
    "connector": "CHARGING",
    "accSessionEnergy": 117,
    "sessionStartValue": 1526126,
    "accSessionMillis": 194674,
    "sessionStartTime": 1671706441943,
    "currentChargingCurrent": 9500,
    "currentChargingPower": 2100,
    "nrOfPhases": 1,
    "twinSerial": 0,
    "cableLockMode": 0,
    "minCurrentLimit": 6,
    "dipSwitchSettings": 7871,
    "cpuType": 2004993,
    "updateable": false
  }
}
```