# chargrr-ui


## Description
A simple UI for, to begin with GARO, chargeboxes.

## Installation

## Usage

## Support

## Roadmap
- [ ] Inital release
- [ ] Turn box on and off through api call
- [ ] Automatically turn a port off if amp drops below 1A
- [ ] Collect statistics, input into noSQL or TSDB.
- [ ] Pincode authentication
- [ ] Monthly reports
- [ ] Try to identify connected vehicle
- [ ] Homeassistant integration
- [ ] Docker deployment

## Contributing
TBD

## Authors and acknowledgment

## License
AGPL v3

## Project status
In early stages, not even an alpha, just a thought in the back of everyones mind.
